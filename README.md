# ROScube ownership example

Ownership is for redundancy nodes. For example, 2 publishers and 1 subscribers, if their ownership qos are `shared`, these 2 publisher shared the ownership to publisher messages.

If they are exclusive with different strength, the higher strength will have the ownership to publisher, and the lower strength will have no ability to publish message to subscriber.

## Shared ownership example

*subscriber side*

```bash
$> ros2 run ownership ownership_sub -k shared
```

*publisher side*

One publisher is shared ownership and it's ID is 100

```bash
$> ros2 run ownership ownership_pub -k shared -i 100
```

One publisher is shared ownership and it's ID is 200

```bash
$> ros2 run ownership ownership_pub -k shared -i 200
```

## Exclusive ownership example

*subscriber side*

```bash
$> ros2 run ownership ownership_sub -k exclusive
```

*publisher side*

One publisher is shared ownership and it's ID is 100

```bash
$> ros2 run ownership ownership_pub -k exclusive -i 100 -s 100
```

One publisher is shared ownership and it's ID is 200

```bash
$> ros2 run ownership ownership_pub -k exclusive -i 200 -s 200
```

Now you can see subscriber only can receive `node id 200` message. And then turn off `node 200`, you will see `node 100` will hand over.
